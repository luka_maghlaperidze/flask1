from flask import Flask, render_template, redirect, url_for

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/about')
def aboutus():
    return render_template('aboutus.html')


@app.route('/staff')
def staff():
    return render_template('staff.html')


if __name__ == '__main__':
    app.run(debug=True)
